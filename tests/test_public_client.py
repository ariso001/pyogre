import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import pytest
import time
from pyogre.public_client import TOPublicClient

pc = TOPublicClient()
test_markets = [('BTC-ETH'), ('BTC-LTC')]

def test_get_markets():
  r = pc.get_markets()
  time.sleep(.25)
  assert type(r) is list
  for i in r:
    assert 'price' in list(i.values())[0]

@pytest.mark.parametrize("x", test_markets)
def test_get_order_book(x):
  r = pc.get_order_book(x)
  time.sleep(.25)
  assert type(r) is dict
  assert r['success'] == 'true'

@pytest.mark.parametrize("x", test_markets)
def test_get_ticker(x):
  r = pc.get_ticker(x)
  time.sleep(.25)
  assert type(r) is dict
  assert r['success'] == True

@pytest.mark.parametrize("x", test_markets)
def test_get_trade_history(x):
  r = pc.get_trade_history(x)
  time.sleep(.25)
  assert type(r) is list
  for i in r:
    assert 'price' in i.keys()

