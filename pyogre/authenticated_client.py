import requests
from pyogre.public_client import TOPublicClient

class TOAuthenticatedClient(TOPublicClient):
    
    def __init__(self, apiuser, apisecret, api_url='https://tradeogre.com/api/v1'):
        """ Provides access to privite Endpoints on the TradeOgre API.
        
        All requests default to the live `api_url`: 'https://tradeogre.com/api/v1'.
        """
        super(TOAuthenticatedClient, self).__init__(api_url)
        self.session = requests.Session()
        self.session.auth = (apiuser, apisecret)
    
    def get_balances(self):
        """ Retrieve all balances for your account.
        
            Returns:
            {"success":true,
            "balances":{"BTC":"13.00419483",
            "XMR":"396.93688709",
            "LTC":"2.00000000",
            "SUMO":"1.00000000",
            "ETN":"0.10000000",
            "AEON":"0.00000000",
            "XVG":"1.00000000",
            "BCN":"2.20000000",
            "FBF":"1.00000000",
            "XAO":"2.00000000",
            "ITNS":"20.00000000"}
            } 
        """
        return self._send_message('get', '/account/balances')
    
    def get_balance(self, currency):
        """ Get the balance of a specific currency for you account. 
            The currency field is required, such as BTC. 
            The total balance is returned and the available balance is what can be used in orders or withdrawn.
            
            Args:
                currency (str): A currency supported in the exchange.
            
            Returns:
                {"success":true,
                "balance":"0.10000000",
                "available":"0.00000000"
                }  
        """
        data = {'currency' : currency}
        return self._send_message('post', '/account/balance', data=data)
    
    def get_orders(self, market):
        """ Retrieve the active orders under your account. 
            The market field is optional, and leaving it out will return all orders in every market. 
            date is a Unix UTC timestamp.
            
            Args:
                market (str): A market supported in the exchange.
            
            Returns:
                {"success":true,
                [
                {"uuid":"a40ac710-8dc5-b5a8-aa69-389715197b14",
                "date":1514876938,"type":"sell",
                "price":"0.02621960",
                "quantity":"1.55772526",
                "market":"BTC-XMR"
                },
                {
                "uuid":"7cbbdbf9-a3a8-d106-c53a-2b17e535580d",
                "date":1514856437,
                "type":"sell",
                "price":"0.02590469",
                "quantity":"1.54412193",
                "market":"BTC-XMR"
                },
                ...
        """ 
        data = {'market' : market}
        return self._send_message('post', '/account/orders', data=data)
    
    def get_order(self, uuid):
        """ Retrieve information about a specific order by the uuid of the order. 
            date is a Unix UTC timestamp. 
            If an order was 100% fulfilled, it will be removed and this api method will return a 'Order not found' error message, so your app must take this into account if it needs to determine if an order was completed.
            
            Args:
                uuid (str): A valid order uuid.
            
            Returns:
                {"success":true,
                "date":"1526503486",
                "type":"sell",
                "market":"BTC-XMR",
                "price":"0.02891990",
                "quantity":"1.00000000",
                "fulfilled":"0.00000000"
                }
        """
        return self._send_message('get', '/account/order/' + uuid)
    
    def cancel_order(self, uuid):
        """ Cancel an order on the order book based on the order uuid. 
            The uuid parameter can also be set to all and all of your orders will be cancelled across all markets.
            
            Args:
                uuid (str): A valid order uuid or 'all' keyword
                
            Returns:
                {
                "success":true
                } 
        """
        data = {'uuid' : uuid}
        return self._send_message('post', '/account/order/cancel', data=data)
    
    def sell(self, market, quantity, price):
        """ Submit a sell order to the order book for a market. 
            The success status will be false if there is an error, and error will contain the error message. 
            Your available buy and sell balance for the market will be returned if successful. 
            If your order is successful but not fully fulfilled, the order is placed onto the order book and you will receive a uuid for the order.
            
            Args:
                market (str): A market supported in the exchange.
                quantity (str): The quantity of coins for the order
                price (str): The price of the order
                
            Returns:
                {
                "success":true,
                "uuid":"235f770b-aa3f-4a31-8194-73d9612c2df1",
                "bnewbalavail":"0.10000000",
                "snewbalavail":"0.50000000"
                } 
        """
        data = {'market' : market, 'quantity' : quantity, 'price' : price}
        return self._send_message('post', '/account/order/sell', data=data)
    
    def buy(self, market, quantity, price):
        """ Submit a buy order to the order book for a market. 
            The success status will be false if there is an error, and error will contain the error message. 
            Your available buy and sell balance for the market will be returned if successful. 
            If your order is successful but not fully fulfilled, the order is placed onto the order book and you will receive a uuid for the order.
        
            Args:
                market (str): A market supported in the exchange.
                quantity (str): The quantity of coins for the order
                price (str): The price of the order
                
            Returns:
                {
                "success":true,
                "uuid":"235f770b-aa3f-4a31-8194-73d9612c2df1",
                "bnewbalavail":"0.10000000",
                "snewbalavail":"0.50000000"
                } 
        """
        data = {'market' : market, 'quantity' : quantity, 'price' : price}
        return self._send_message('post', '/account/order/buy', data=data)
    
    


