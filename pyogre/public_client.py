import requests

class TOPublicClient(object):
    """TO public client API.
    All requests default to the `product_id` specified at object
    creation if not otherwise specified.
    Attributes:
        url (Optional[str]): API URL. Defaults to cbpro API.
    """
    
    def __init__(self, api_url='https://tradeogre.com/api/v1', timeout=30):
        """Create tradeogre API public client.
        Args:
            api_url (Optional[str]): API URL. Defaults to cbpro API.
        """
        self.url = api_url.rstrip('/')
        self.auth = None
        self.session = requests.Session()
    
    def get_markets(self):
        """Retrieve a listing of all markets and basic information including current price, volume, high, low, bid and ask.
        
        Returns:
            list: Info about all currency pairs. Example::
                [
                    {
                        "id": "BTC-USD",
                        "display_name": "BTC/USD",
                        "base_currency": "BTC",
                        "quote_currency": "USD",
                        "base_min_size": "0.01",
                        "base_max_size": "10000.00",
                        "quote_increment": "0.01"
                    },
                    ...
                ]
        """
        return self._send_message('get', '/markets')
    
    def get_order_book(self, market):
        """ Retrieve the current order book for {market} such as BTC-XMR.
            
            Args:
                market (str): A market supported in the exchange.
                
            Returns:
                {"success":true,
                "buy":{"0.02425501":"36.46986607",
                "0.02425502":"93.64201137",
                "0.02425503":"19.02000000",
                ...}
                "sell":{"0.02427176":"737.34633975",
                "0.02427232":"94.30483300",
                ...}
                }
                    
        """
        return self._send_message('get', '/orders/{}'.format(market)) 
    
    def get_ticker(self, market):
        """ Retrieve the ticker for {market}, volume, high, and low are in the last 24 hours, initialprice is the price from 24 hours ago.
            
            Args: 
                market (str): A market supported in the exchange.
                
            Returns:
                {"success":true,
                "initialprice":"0.02502002",
                "price":"0.02500000",
                "high":"0.03102001",
                "low":"0.02500000",
                "volume":"0.15549958",
                "bid":"0.02420000",
                "ask":"0.02625000"
                } 
        """    
        return self._send_message('get', '/ticker/{}'.format(market))
    
    def get_trade_history(self, market):
        """ Retrieve the history of the last trades on {market} limited to 100 of the most recent trades. 
            The date is a Unix UTC timestamp.
            
            Args:
                market (str): A market supported in the exchange.
                
            Returns:
                [
                {"date":1515128233,
                "type":"sell",
                "price":"0.02454320",
                "quantity":"0.17614230"
                },
                ...
                ]
            """
        return self._send_message('get', '/history/{}'.format(market))
    
    def _send_message(self, method, endpoint, params=None, data=None, apiuser=None, apisecret=None):
        """Send API request.
        Args:
            method (str): HTTP method (get, post, delete, etc.)
            endpoint (str): Endpoint (to be added to base URL)
            params (Optional[dict]): HTTP request parameters
            data (Optional[str]): JSON-encoded string payload for POST
        Returns:
            dict/list: JSON response
        """
        url = self.url + endpoint
        r = self.session.request(method, url, params=params, data=data,
                                 auth=self.auth, timeout=30)
        return r.json()


