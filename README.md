# Pyogre

A Python client for the [TradeOgre API](https://tradeogre.com/help/api)  
This is a super lightweight wrapper for the TradeOgre APIs.
The only depency is "requests" module.

##Disclaimer
I'm not affiliated, associated, authorized, endorsed by, or in any way officially connected with TradeOgre.
This is a pure personal project to learn some new stuff.


##### Provided under MIT License by Daniel Paquin.
MIT License – please take the following message to heart:*
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Under Heavy Development
This is my first public and open source python project.  
I'm not a developer nor pretend to be one.  
Therefore be very very carefull and prepared for disasters :)

## Status
Pre-Alpha, things might or might not work.
Developing of what one day will be a pip package is at an embrional stage.
This can change at any time without any notice and might be not retro compatible.

##TODO
- Use brain
- Update in-function documentations to be in line with TO api docs - **Done**
- TESTS!!!  
  - public_client tests - done  
  - auth_client tests
- Create documentation and examples
- Create installation package
- Error handling
